
//app.js
//exercise 1
console.log("Hello world!");

//exercise 2
const myAge = 19;

if(myAge >= 21){
    console.log("You have an existential crisis");
}
else if (myAge < 21 && myAge >= 18){
    console.log("Enjoy while it last!");
}
else{
    console.log("You're not an adult yet!");
}

//exercise 3
const lecture = 1;
const lab = 2;

//if lab = 0 then 2nd else if
//if lecture = 0 then 4th else if
//if both are 0 then 3rd else if
if(lecture + lab == 3){
    console.log("Passed the lecture and lab");
}
else if (lecture - lab == 1){
    console.log("Passed the lecture but not lab");
}
else if(lab + lecture == 0){
    console.log("Neither pass lecture nor lab");
}
else if (lab - lecture != 1){
    console.log("Passed the lab but not lecture");
}
else{
    console.log("");
}

//exercise 4
let height = 5
let star = ""

for (let s = 1; s <= height; s++){
  for (let i = 1; i <= s; i++){
     star += "*"
  }
   star += "\n"
   //console.log(star)
}
console.log(star);

//exercise 5

function distance(x1, y1, x2, y2){
    let y = x2 - x1;
    let x = y2 - y1;
    eq1 = Math.sqrt(x * x + y * y);
    console.log(eq1)
}
distance(5,6,8,9)

//exercise 6

function distanceobj(point1, point2) {
    let y = point2.x - point1.x;
    let x = point2.y - point1.y;
    const eq2 = Math.sqrt(x * x + y * y);
    console.log(eq2);
  }
  const point1 = { x: 0, y: 0 };
  const point2 = { x: 3, y: 4 };
  distanceobj(point1, point2);
    

//exercise 7
let Array = [];
for (let s = 2; s <= 11; s++) {
  Array.push(s);
}
console.log(Array);
